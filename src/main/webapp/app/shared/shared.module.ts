import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RavikumarGujjulaSharedLibsModule, RavikumarGujjulaSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [RavikumarGujjulaSharedLibsModule, RavikumarGujjulaSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [RavikumarGujjulaSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RavikumarGujjulaSharedModule {
  static forRoot() {
    return {
      ngModule: RavikumarGujjulaSharedModule
    };
  }
}
